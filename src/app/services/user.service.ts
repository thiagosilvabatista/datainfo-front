import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from './../../environments/environment';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAllUsers() {
    return this.http.get<any[]>(
    environment.api + "external-user"
    );
  }

  getUsersFilter(userName: string[], situation: string[], perfil: number[]){    
    let parameters = '';

    if(userName != null && userName[0]){
      parameters += "&userName="+ userName.join(',')
    }
    if(situation != null && situation[0] && situation[0] !== '-1'){
      parameters += "&situation=" + situation.join(',');
    }

    if(perfil != null && perfil[0]){
      parameters += "&perfil=" + perfil.join(',');
    }   

    return this.http.get<any>(
      environment.api + 'external-user/filter?' + parameters
    );
  }

  insertUser(user: any) {
    return this.http.post(environment.api + 'external-user/', user);
  }

  getExternalUserFunction() {
    return this.http.get<any>(
      environment.api + 'external-user-function'
    );
  } 

  getUserById(numIdUser) {
    return this.http.get<any[]>(
    environment.api + `external-user/${numIdUser}`
    );
  }

  updateUser(user: any) {    
    return this.http.post(environment.api + 'external-user/'+user.numId, user);
  }

  updateUserSituation(numId: any, strSituation: any) {    
    return this.http.post(environment.api + `external-user/${numId}/${strSituation}`, {});
  }

  deleteUser(numId: any) {    
    return this.http.delete(environment.api + `external-user/${numId}`, {responseType: 'text'});
  }

}
