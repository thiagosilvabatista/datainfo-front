import { DialogComponent } from './../../share/dialog/dialog.component';
import { MatDialog } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from './../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  numIdUser: any;

  public maskFone = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskCpf = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];

  userEdit: any;
  user = <any> {};
  /*user = {
    numId: -1,
    strCpf: '',
    strName: '',
    strEmail: '',
    strSituation: '-1',
    numPerfilAccess: -1,
    strFone: '',
    numExternalUserFunction: -1,
  };*/

  externalUserFunctionList: any;
  subscriptionForm: FormGroup;

  enableButton = false;

  constructor(public dialog: MatDialog, private fb: FormBuilder, 
              private activatedRoute: ActivatedRoute, private userService: UserService,
               private router: Router) { 
    
    this.userEdit = activatedRoute.snapshot.paramMap.get('numId');    
    this.getAllExternalUserFunction();
    this.getUserById(this.userEdit);

    this.subscriptionForm = fb.group({
      userStrSituation: ['', Validators.required],
      userId: ['', Validators.required],
      userEmail: ['', Validators.required],
      userName: ['', Validators.required],
      userCpf: ['', Validators.required],
      userFone: ['', Validators.required],
      userFunction: ['', Validators.required],
      userProfile: ['', Validators.required]
    });

  }

  ngOnInit() {

    this.enableButton = this.subscriptionForm.status === 'VALID';

  } 

  getUserById(numIdUser: number){
    this.userService.getUserById(numIdUser).subscribe(data => {
      this.user.numId = data['numId'];
      this.user.strCpf =  data['strCpf'];
      this.user.strName = data['strName'];
      this.user.strEmail = data['strEmail'];
      this.user.strSituation = data['strSituation'];
      this.user.numPerfilAccess = data['numPerfilAccess'].toString();
      this.user.strFone = data['strFone'];
      this.user.numExternalUserFunction = data['externalUserFunction']['numFunction'].toString();    

      this.subscriptionForm.controls['userId'].setValue(this.user.numId);
      this.subscriptionForm.controls['userCpf'].setValue(this.user.strCpf);
      this.subscriptionForm.controls['userName'].setValue(this.user.strName);
      this.subscriptionForm.controls['userEmail'].setValue(this.user.strEmail);
      this.subscriptionForm.controls['userStrSituation'].setValue(this.user.strSituation);
      this.subscriptionForm.controls['userProfile'].setValue(this.user.numPerfilAccess);
      this.subscriptionForm.controls['userFone'].setValue(this.user.strFone);
      this.subscriptionForm.controls['userFunction'].setValue(this.user.numExternalUserFunction);
    });
  }

  getAllExternalUserFunction(){
    this.userService.getExternalUserFunction().subscribe(data => {
      this.externalUserFunctionList = data;      
    })
  }

  home() {
    this.router.navigate(['']);        
  }

  onSubmit(){
    if(this.subscriptionForm.status === 'VALID' && this.validateFields(this.subscriptionForm)) {
      this.user.strCpf =  this.subscriptionForm.value.userCpf.match(/\d+/g).join('');
      this.user.strName = this.subscriptionForm.value.userName;
      this.user.strEmail = this.subscriptionForm.value.userEmail;
      this.user.strSituation = 'A';
      this.user.numPerfilAccess = parseInt(this.subscriptionForm.value.userProfile);
      this.user.strFone = this.subscriptionForm.value.userFone.match(/\d+/g).join('');
      this.user.numExternalUserFunction = parseInt(this.subscriptionForm.value.userFunction);
    
      this.userService.updateUser(this.user).subscribe(data => {
        this.openDialog();
        this.router.navigate(['']);        
      }, erro => {        
        this.openDialogError(erro.message);
      });
  
    } else {
      this.openDialogError('Por favor preencha todos dos dados solicitados.');
    }
  }

  validateFields(subscriptionForm) {
    if(!subscriptionForm.value.userCpf || subscriptionForm.value.userCpf.match(/\d+/g).join('').length !== 11) {
      subscriptionForm.controls.userCpf.status = 'INVALID';
      return false;
    } 
    if(!subscriptionForm.value.userFone || subscriptionForm.value.userFone.match(/\d+/g).join('').length !== 11) {
      subscriptionForm.controls.userFone.status = 'INVALID';
      return false;
    }
    return true;
  }

  enableButtonFunction() {
    this.enableButton = this.subscriptionForm.status === 'VALID';
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: "Usuário editado com sucesso."
    });
  }

  openDialogError(messageErro: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: messageErro
    });
  }

}
