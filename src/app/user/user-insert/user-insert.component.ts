import { Router } from '@angular/router';
import { DialogComponent } from './../../share/dialog/dialog.component';
import { UserService } from './../../services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-user-insert',
  templateUrl: './user-insert.component.html',
  styleUrls: ['./user-insert.component.css']
})
export class UserInsertComponent implements OnInit {

  animal: string;
  name: string;

  public maskFone = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskCpf = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];

  user = {
    strCpf: null,
    strName: null,
    strEmail: null,
    strSituation: null,
    numPerfilAccess: null,
    strFone: null,
    numExternalUserFunction: null,
  };

  externalUserFunctionList: any;

  subscriptionForm: FormGroup;

  enableButton = false;

  constructor(private fb: FormBuilder, private userService: UserService, 
              public dialog: MatDialog, private router: Router) {

    this.subscriptionForm = fb.group({
      userEmail: ['', Validators.required],
      userName: ['', Validators.required],
      userCpf: ['', Validators.required],
      userFone: ['', Validators.required],
      userFunction: ['', Validators.required],
      userProfile: ['', Validators.required]
    });

   }

  ngOnInit() {
    this.enableButton = this.subscriptionForm.status === 'VALID';
    this.getAllExternalUserFunction();
  }

  onSubmit() {
    if(this.subscriptionForm.status === 'VALID' && this.validateFields(this.subscriptionForm)) {
      this.user.strCpf =  this.subscriptionForm.value.userCpf.match(/\d+/g).join('');
      this.user.strName = this.subscriptionForm.value.userName;
      this.user.strEmail = this.subscriptionForm.value.userEmail;
      this.user.strSituation = 'A';
      this.user.numPerfilAccess = parseInt(this.subscriptionForm.value.userProfile);
      this.user.strFone = this.subscriptionForm.value.userFone.match(/\d+/g).join('');
      this.user.numExternalUserFunction = parseInt(this.subscriptionForm.value.userFunction);
    
      this.userService.insertUser(this.user).subscribe(data => {
        this.openDialog();
        this.router.navigate(['']);        
      }, erro => {        
        this.openDialogError(erro.error.errors[0].defaultMessage);
      });
  
    } else {
      this.openDialogError('Por favor preencha todos dos dados solicitados.');
    }

  }

  home() {
    this.router.navigate(['']);        
  }

  validateFields(subscriptionForm) {
    if(!subscriptionForm.value.userCpf || subscriptionForm.value.userCpf.match(/\d+/g).join('').length !== 11) {
      subscriptionForm.controls.userCpf.status = 'INVALID';
      return false;
    } 
    if(!subscriptionForm.value.userFone || subscriptionForm.value.userFone.match(/\d+/g).join('').length !== 11) {
      subscriptionForm.controls.userFone.status = 'INVALID';
      return false;
    }
    return true;
  }

  enableButtonFunction() {
    this.enableButton = this.subscriptionForm.status === 'VALID';
  }

  getAllExternalUserFunction(){
    this.userService.getExternalUserFunction().subscribe(data => {
      this.externalUserFunctionList = data;      
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: "Usuário inserido com sucesso."
    });
  }

  openDialogError(messageErro: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: messageErro
    });
  }



}
