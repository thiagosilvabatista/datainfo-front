import { DialogOptionsComponent } from './../../share/dialog-options/dialog-options.component';
import { DialogComponent } from './../../share/dialog/dialog.component';
import { UserService } from './../../services/user.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import * as _ from 'lodash';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, AfterViewInit {

  situations = [
    {
      text: '',
      value: '-1'
    },
    {
      text: 'Ativo',
      value: 'A'
    },
    {
      text: 'Inativo',
      value: 'I'
    }
  ]

  profiles = [
    {
      text: '',
      value: '-1'
    },
    {
      text: 'Aluno',
      value: '0'
    },
    {
      text: 'Gestor Municipal',
      value: '1'
    },
    {
      text: 'Gestor Estadual',
      value: '2'
    },
    {
      text: 'Gestor Nacional',
      value: '3'
    }
  ]

  displayedColumns: string[] = ['email', 'name', 'perfil', 'enabled', 'edit', 'delete'];
  dataSource;
  subscriptionForm: FormGroup;
  user:any;
  users:any;

  userName = []; 
  situation = [];
  perfil = [];

  filterUserName: any;
  filterSituation: any;
  filterPerfil: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  min_badrooms;

  constructor(public dialog: MatDialog, private router: Router, 
              private userService: UserService, 
              private fb: FormBuilder) {
    this.getAllUsers();
      this.subscriptionForm = fb.group({
      userName: [''],
      situation: [''],
      perfil: ['']
    });    
   }

  ngAfterViewInit(){     
    this.filterUserName = sessionStorage.getItem('filterUserName');
    this.filterSituation = sessionStorage.getItem('filterSituation');
    this.filterPerfil = sessionStorage.getItem('filterPerfil');

    this.userName.push(this.filterUserName);
    this.situation.push(this.filterSituation);
    this.perfil.push(this.filterPerfil);
    this.OnSubmit();
  }

  ngOnInit() {
    
   }

  OnSubmit() {
    this.user = this.subscriptionForm.value;
    this.userName = []; 
    this.situation = [];
    this.perfil = [];
    
    this.userName.push((this.user.userName === '' ? 
                        (sessionStorage.getItem('filterUserName') !== '' ? sessionStorage.getItem('filterUserName') : '') : 
                        this.user.userName));

    this.situation.push((this.user.situation === '' ? 
                        (sessionStorage.getItem('filterSituation') !== '' ? sessionStorage.getItem('filterSituation') : '') : 
                        this.user.situation));

    this.perfil.push((this.user.perfil === '' ?  
                      (sessionStorage.getItem('filterPerfil') !== '' ? sessionStorage.getItem('filterPerfil') : '' ) : 
                      this.user.perfil));
    this.getUsersFilter();
  }

  getAllUsers(){
    this.userService.getAllUsers().subscribe(data => {
      this.users = _.orderBy(data, ['numId'], ['desc']);
      this.dataSource = new MatTableDataSource<any>(this.users);
      this.dataSource.paginator = this.paginator;
    })
  }

  getUsersFilter(){
    this.setSessionStorageFilter(this.userName, this.situation, this.perfil);   
    this.userService.getUsersFilter(this.userName, this.situation, this.perfil).subscribe(data => {
      this.users = _.orderBy(data, ['numId'], ['desc']);
      this.dataSource = new MatTableDataSource<any>(this.users);
      this.dataSource.paginator = this.paginator;
    })
  }

  setSessionStorageFilter(userName: string[], situation: string[], perfil: number[]) {
    if(userName && userName[0]) {
      sessionStorage.setItem('filterUserName', userName[0]);
      this.filterUserName = sessionStorage.getItem('filterUserName');
    }else {
      sessionStorage.setItem('filterUserName', '');
      this.filterUserName = sessionStorage.getItem('filterUserName');
    }

    if(situation && situation[0]) {
      sessionStorage.setItem('filterSituation', situation[0]);
      this.filterSituation = sessionStorage.getItem('filterSituation');
    }else {
      sessionStorage.setItem('filterSituation', '-1');
      this.filterSituation = sessionStorage.getItem('filterSituation');
    }
    
    if(perfil && perfil[0]) {
      sessionStorage.setItem('filterPerfil', perfil[0].toString());
      this.filterPerfil = sessionStorage.getItem('filterPerfil');
    }else {
      sessionStorage.setItem('filterPerfil', '-1');
      this.filterPerfil = sessionStorage.getItem('filterPerfil');
    }
  }

  maskCpf(value) {
    return value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g,"\$1.\$2.\$3\-\$4");
  }

  getPerfil(value: number): string{
    if(value === 0){
      return 'Aluno'
    }if(value === 1){
      return 'Gestor Municipal';
    }if(value === 2){
      return 'Gestor Estadual';
    }if(value === 3){
      return 'Gestor Nacional';
    }
  }

  onSubmit(form) {
    
  }

  updateSituation(strSituation: any) {
    return strSituation === 'A' ? 'I' : 'A';
  }

  enableAndDisable(numId: any, strSituation: any){
    const newSituation = this.updateSituation(strSituation)
    this.userService.updateUserSituation(numId, newSituation).subscribe(data => {
        this.openDialogMessage('Modificação realizada com sucesso.');
        this.getAllUsers();       
      }, erro => {        
        this.openDialogMessage(erro.message);
        this.getAllUsers();
      });    
  }

  edit(numId: any){
    this.router.navigate(['/user-edit/'+numId]);
  }

  delete(numId: any, strName: any){
    this.openDialogOptions(`Deseja deseja deletar o usuario ${strName}`, numId);
  }

  openFormInsertUser(){
    this.router.navigate(['/user-insert']);
  }

  setFilterUserName(event){
    sessionStorage.setItem('filterUserName', event.target.value);
  }

  setFilterSituation(event) {
    sessionStorage.setItem('filterSituation', event.value);
  }

  setFilterPerfil(event) {
    sessionStorage.setItem('filterPerfil', event.value);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: "Usuário inserido com sucesso."
    });
  }

  openDialogMessage(messageErro: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: messageErro
    });
  }

  openDialogOptions(messageErro: string, numId): void {
    const dialogRef = this.dialog.open(DialogOptionsComponent, {
      width: '350px',
      data: messageErro
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.userService.deleteUser(numId).subscribe(data => {
          this.openDialogMessage('Usuario deletado com sucesso.');
          this.getAllUsers();       
        }, erro => {        
          this.openDialogMessage(erro.message);
          this.getAllUsers();
        });           
      }
    })
  }

}
